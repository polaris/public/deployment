```mermaid
graph TD;
subgraph Polaris
Z[Docker Build]
end
Z[Docker Build] --> A(Ingress);
Z[Docker Build] --> B(Ingress);
subgraph GitLab
B[Container Registry] --> H(Images);
end
subgraph Server
A[Docker Pull] --> D(Traefik);
D[Traefik] --> E(Deployment:Analytics-Engine);
D[Traefik] --> F(Deployment:Rights-Engine);
D[Traefik] --> I(Deployment:Dashboard-SDK);
E(Deployment:Analytics-Engine)---id1[(MongoDB)]
F(Deployment:Rights-Engine)---id1[(MongoDB)]
I(Deployment:Dashboard-SDK)---id1[(MongoDB)]
end
```
# Getting started 
Each folder contains one of the components of 

## Login in the docker registry 
If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. If you have Two-Factor Authentication enabled, use a Personal Access Token instead of a password.
```bash
docker login registry.digitallearning.gmbh
```

## LRS / MongoDB
First we need a database or LRS to store the xAPI Statements. In a local setup, you can use the docker container in the mongodb folder. 
Rename or copy the sample configuration. Please change all passwords in the configuration file. 

```bash
cd mongodb
cp .env.sample .env
```
Now you can start the mongodb with the following command
```bash
docker compose up -d
```

Please check that the database is created:
```bash
docker compose ps
```


## Rights engine
First create some jwt keys with the following command:

```bash
cd rights-engine
ssh-keygen -t rsa -b 4096 -m PEM -f id_rsa
openssl rsa -in id_rsa -pubout -outform PEM -out id_rsa.pub
```

Leave the passphrase empty. 
Rename or copy the sample configuration. Please change all passwords in the configuration file. 
```bash
cp .env.sample .env
```

Now you can start the rights engine with the following command
```bash
docker compose up -d
```

Please check that all services are started:
```bash
docker compose ps
```

After that you should be able to visit http://localhost:80/ and see the rights engine.

## Analytics engine
Rename or copy the sample configuration. Please change all passwords in the configuration file. 
```bash
cd analytics-engine
cp .env.sample .env
```

Now you can start the analytics engine with the following command
```bash
docker compose up -d
```

## Update Docker Images 
```bash
docker compose pull
```